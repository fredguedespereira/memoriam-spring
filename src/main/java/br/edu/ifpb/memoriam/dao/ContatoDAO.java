package br.edu.ifpb.memoriam.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Usuario;
import br.edu.ifpb.pweb2.genericdao.GenericJpaDao;

@Repository
public class ContatoDAO extends GenericJpaDao<Contato, Integer> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	public List<Contato> findAllFromUser(Usuario usuario) {
		Query q = this.entityManager.createQuery("from Contato c where c.usuario = :usuario");
		q.setParameter("usuario", usuario);
		return q.getResultList();
	}

	

}
