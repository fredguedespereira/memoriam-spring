package br.edu.ifpb.memoriam.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.edu.ifpb.memoriam.entity.Usuario;
import br.edu.ifpb.pweb2.genericdao.GenericJpaDao;

@Repository
public class UsuarioDAO extends GenericJpaDao<Usuario, Integer>{

	public Usuario findByLogin(String email) {
		Query q = this.entityManager.createQuery("from Usuario u where u.email = :email");
		q.setParameter("email", email);
		return (Usuario) q.getSingleResult();
	}

}
