package br.edu.ifpb.memoriam.dao;

import org.springframework.stereotype.Repository;

import br.edu.ifpb.memoriam.entity.Operadora;
import br.edu.ifpb.pweb2.genericdao.GenericJpaDao;

@Repository
public class OperadoraDAO extends GenericJpaDao<Operadora, Integer>{

	
}
