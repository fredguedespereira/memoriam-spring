package br.edu.ifpb.memoriam.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.memoriam.dao.OperadoraDAO;
import br.edu.ifpb.memoriam.entity.Operadora;

@Controller
@RequestMapping("/operadoras")
public class OperadoraController {

	@Autowired
	OperadoraDAO odao;
	
	@Autowired
	HttpSession httpSession;

	@RequestMapping("/form")
	public String form(Model model, RedirectAttributes flash) {
		model.addAttribute("operadora", new Operadora());
		return "operadoras/form";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String cadastrar(Operadora operadora) {
		if (operadora.getId() == null) {
			odao.save(operadora);
		} else {
			odao.update(operadora);
		}
		return "redirect:/operadoras";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String consultar(Model model) {
		List<Operadora> operadoras = odao.findAll();
		model.addAttribute("operadoras", operadoras);
		return "/operadoras/listagem";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String buscar(@PathVariable("id") Integer id, Model model) {
		Operadora operadora = odao.findById(id);
		model.addAttribute("operadora", operadora);
		return "/operadoras/form";
	}

	@Transactional
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String excluir(Integer delids[], Model model) {
		for (Integer id : delids) {
			Operadora operadora = odao.findById(id);
			odao.delete(operadora);
		}
		return "redirect:/operadoras";
	}
}
