package br.edu.ifpb.memoriam.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.memoriam.dao.ContatoDAO;
import br.edu.ifpb.memoriam.dao.OperadoraDAO;
import br.edu.ifpb.memoriam.dao.UsuarioDAO;
import br.edu.ifpb.memoriam.entity.Contato;
import br.edu.ifpb.memoriam.entity.Operadora;
import br.edu.ifpb.memoriam.entity.Usuario;

@Controller
@RequestMapping("/contatos")
public class ContatoController {

	@Autowired
	ContatoDAO cdao;

	@Autowired
	UsuarioDAO udao;

	@Autowired
	OperadoraDAO odao;

	@Autowired
	HttpSession httpSession;

	@RequestMapping("/form")
	public String form(Model model, RedirectAttributes flash) {
		model.addAttribute("contato", new Contato());
		model.addAttribute("operadoraOptions", this.getOperadorasOptions());
		return "contatos/form";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String cadastrar(Contato contato, Integer operadoraId) {
		Usuario usuarioLogado = (Usuario) httpSession.getAttribute("usuario");
		Operadora operadora = odao.findById(operadoraId);
		contato.setUsuario(usuarioLogado);
		contato.setOperadora(operadora);
		if (contato.getId() == null) {
			cdao.save(contato);
		} else {
			cdao.update(contato);
		}
		return "redirect:/contatos";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String consultar(Model model) {
		Usuario usuarioLogado = (Usuario) httpSession.getAttribute("usuario");
		List<Contato> contatos = cdao.findAllFromUser(usuarioLogado);
		model.addAttribute("contatos", contatos);
		return "/contatos/listagem";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String buscar(@PathVariable("id") Integer id, Model model) {
		Contato contato = cdao.findById(id);
		model.addAttribute("contato", contato);
		model.addAttribute("operadoraOptions", this.getOperadorasOptions());
		return "/contatos/form";
	}

	@Transactional
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String excluir(Integer delids[], Model model) {
		for (Integer id : delids) {
			Contato contato = cdao.findById(id);
			cdao.delete(contato);
		}
		return "redirect:/contatos";
	}

	public Map<Integer, String> getOperadorasOptions() {
		List<Operadora> operadorasList = odao.findAll();
		Map<Integer, String> operadoras = operadorasList.stream()
				.collect(Collectors.toMap(Operadora::getId, Operadora::getNome));
		return operadoras;
	}

}
